$(function() {

	$('.tombolTambahData').on('click', function() {

		$('#formModalLabel').html('Tambah Data Mahasiswa');
		$('.modal-footer button[type=submit]').html('Tambah Data');

	});

});

	$('.tampilModalUbah').on('click', function() {

		$('#formModalLabel').html('Ubah Data Mahasiswa');
		$('.modal-footer button[type=submit]').html('Ubah Data');
		$('.modal-body form').attr('action', 'http://localhost/phpmvc/public/mahasiswa/ubah');

		const id = $(this).data('id');

		$.ajax({
			url: 'http://localhost/phpmvc/public/mahasiswa/getubah',
			data: {id : id},
			method: 'post',
			datatype: 'json',
			success: function(data) {
				var obj = $.parseJSON(data);
				$('#nama').val(obj.nama);
				$('#nrp').val(obj.nrp);
				$('#email').val(obj.email);
				$('#jurusan').val(obj.jurusan);
				$('#id').val(obj.id);
			}
		});

	});
